#ifndef CVIKO1ZUZKA_H
#define CVIKO1ZUZKA_H

#include <QtWidgets/QDialog>
#include "ui_cviko1zuzka.h"
#include <QMessageBox>

class cviko1zuzka : public QDialog
{
	Q_OBJECT

public:
	cviko1zuzka(QWidget *parent = 0);
	~cviko1zuzka();

	public slots:
	void slot1();

private:
	Ui::cviko1zuzkaClass ui;	// ui = user interface
	QMessageBox msgBox;
};

#endif // CVIKO1ZUZKA_H