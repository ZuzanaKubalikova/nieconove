/********************************************************************************
** Form generated from reading UI file 'cviko1zuzka.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CVIKO1ZUZKA_H
#define UI_CVIKO1ZUZKA_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QPushButton>

QT_BEGIN_NAMESPACE

class Ui_cviko1zuzkaClass
{
public:
    QPushButton *pushButton;

    void setupUi(QDialog *cviko1zuzkaClass)
    {
        if (cviko1zuzkaClass->objectName().isEmpty())
            cviko1zuzkaClass->setObjectName(QStringLiteral("cviko1zuzkaClass"));
        cviko1zuzkaClass->resize(600, 400);
        pushButton = new QPushButton(cviko1zuzkaClass);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(50, 40, 91, 91));

        retranslateUi(cviko1zuzkaClass);
        QObject::connect(pushButton, SIGNAL(clicked()), cviko1zuzkaClass, SLOT(slot1()));

        QMetaObject::connectSlotsByName(cviko1zuzkaClass);
    } // setupUi

    void retranslateUi(QDialog *cviko1zuzkaClass)
    {
        cviko1zuzkaClass->setWindowTitle(QApplication::translate("cviko1zuzkaClass", "cviko1zuzka", 0));
        pushButton->setText(QApplication::translate("cviko1zuzkaClass", "Trpaslik", 0));
    } // retranslateUi

};

namespace Ui {
    class cviko1zuzkaClass: public Ui_cviko1zuzkaClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CVIKO1ZUZKA_H
